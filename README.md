## Run locally

Install [python-gitlab](https://github.com/python-gitlab/python-gitlab) ([documentation](https://python-gitlab.readthedocs.io/en/stable/):

`sudo pip3 install python-gitlab`

Create a file `htbin/config.py` and place in it a single line, `private_token = "YourPersonalAccessToken"` (create your secret (do not commit this; this is why config.py is ignored by Git) GitLab personal access token at https://gitlab.com/profile/personal_access_tokens

Now run a simple local server from the command line with:

`python3 -m http.server --cgi`

Visit http://localhost:8000/htbin/merge-requests.py

## Update on server

`scp -r python-gitlab/ agaric-test:~`
`ssh agaric-test`
`sudo cp -r ~/python-gitlab/* /srv/gitlab/`
